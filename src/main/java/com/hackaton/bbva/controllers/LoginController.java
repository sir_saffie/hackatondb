package com.hackaton.bbva.controllers;

import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.models.UserModel;
import com.hackaton.bbva.services.LoginService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hackaton/v1")
public class LoginController {
    @Autowired
    LoginService loginService;

    @ApiOperation(value = "El getLogins (GET /login) devuelve los usuarios logeados")
    @GetMapping("/login")
    public ResponseEntity<ResultModel> getLogins(){
        System.out.println("LoginController - getLogins");
        ResultModel result = new ResultModel();
        result.setObjectModel(loginService.findAll());
        result.setResultType(0);
        result.setStatus(HttpStatus.OK);
        return new ResponseEntity<>(result, result.getStatus());
    }
    @ApiOperation(value = "El createLogin (POST /login) autentifica al usuario y genera su login - token")
    @PostMapping("/login")
    public ResponseEntity<ResultModel> createLogin(@RequestBody UserModel userLogin){
        System.out.println("Controller - createLogin");

        ResultModel result  = loginService.createLogin(userLogin);
        return new ResponseEntity<>(result, result.getStatus());
    }
}
