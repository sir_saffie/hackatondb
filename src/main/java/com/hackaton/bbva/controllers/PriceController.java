package com.hackaton.bbva.controllers;

import com.hackaton.bbva.models.PriceModel;
import com.hackaton.bbva.services.PriceService;
import com.hackaton.bbva.models.ResultModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/hackaton/v1")
public class PriceController {
    @Autowired
    PriceService priceService;

    @GetMapping("/prices")
    public ResponseEntity<List<PriceModel>> getPrices() {
        System.out.println("PriceController.getPrices");

        return new ResponseEntity<>(this.priceService.findAllPrices(), HttpStatus.OK);
    }

    @PostMapping("/prices")
    public ResponseEntity<ResultModel> addPrice(@RequestBody PriceModel price) {
        System.out.println("PriceController.addPrice");

        ResultModel resultModel = this.priceService.addPrice(price);
        return new ResponseEntity<>(resultModel, resultModel.getStatus());
    }

    @GetMapping("/prices/{productId}")
    public ResponseEntity<ResultModel> getPricesByProductId(@PathVariable String productId) {
        System.out.println("producto");
        ResultModel resultModel = this.priceService.findPricesByProductId(productId);
        return new ResponseEntity<>(resultModel, resultModel.getStatus());
    }

    @GetMapping("/prices/{productId}/{fecha}")
    public ResponseEntity<ResultModel> getPricesByProductIdAndDate(@PathVariable String productId, @PathVariable String fecha) {
        System.out.println("producto" + productId);
        System.out.println("fecha:" + fecha);

        ResultModel resultModel = this.priceService.findProductPricesBetweenDates(productId,fecha,"2022-01-01");
        return new ResponseEntity<>(resultModel, resultModel.getStatus());
    }
}
