package com.hackaton.bbva.controllers;

import com.hackaton.bbva.models.LoginModel;
import com.hackaton.bbva.models.ProductInputModel;
import com.hackaton.bbva.models.ProductModel;
import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.services.LoginService;
import com.hackaton.bbva.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v1")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    LoginService loginService;

    @GetMapping("/products")
    public ResponseEntity<ResultModel> getProduct(@RequestHeader String token) {
        System.out.println("getProducts");
        ResultModel resultModel = new ResultModel();
        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null) {
            return new ResponseEntity<>(this.productService.findAllProduct(), HttpStatus.OK);
        } else {
            resultModel.setObjectModel("USUARIO NO LOGUEADO");
            resultModel.setStatus(HttpStatus.FORBIDDEN);
            return  new ResponseEntity<>(resultModel,resultModel.getStatus());
        }
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<ResultModel> getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es: ");

        Optional<ProductModel> producto = this.productService.findProductById(id);
        ResultModel resultModel = new ResultModel();
        resultModel.setObjectModel(producto);
        resultModel.setStatus(HttpStatus.OK);
        return new ResponseEntity<>(resultModel, resultModel.getStatus());
    }

    @PostMapping("/products")
    public ResponseEntity<ResultModel> addProduct(@RequestBody ProductInputModel product) {
        System.out.println("addProduct");
        System.out.println("La id del producto a añadir es: " + product.getId());
        System.out.println("La descripción del producto es: " + product.getDescripcion());
        System.out.println("La categoria del producto es: " + product.getCategoria());
        System.out.println("El valor del producto es: " + product.getValor());
        System.out.println("El precio del producto es: " + product.getPrice());

        ResultModel resultModel = this.productService.addProduct(product);
        return new ResponseEntity<>(resultModel, resultModel.getStatus());
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ResultModel> updateProduct(@RequestBody ProductInputModel product) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar es: " + product.getId());

        ResultModel result = this.productService.updateProductById(product);
        return new ResponseEntity<>(result, result.getStatus());
    }

    @GetMapping("/products/historial/{id}")
    public ResponseEntity<ResultModel> getHistorialPrice(@PathVariable String id,@RequestHeader String token,@RequestParam(value="start_date") String start_date, @RequestParam(value="end_date", defaultValue="2999-12-31") String end_date){
        System.out.println("ProductController - getHistorialPrice");

        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null && !login.getProfile().equalsIgnoreCase("USUARIO")){
            ResultModel result = productService.getHistoricalPrice(id,start_date,end_date);
            return new ResponseEntity<>(result, result.getStatus());
        }else{
            ResultModel result = new ResultModel();
            result.setResultType(9);
            result.setStatus(HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(result, result.getStatus());
        }
    }
}