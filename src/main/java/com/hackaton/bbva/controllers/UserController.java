package com.hackaton.bbva.controllers;


import com.hackaton.bbva.models.LoginModel;
import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.models.UserModel;
import com.hackaton.bbva.services.LoginService;
import com.hackaton.bbva.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;

@RestController
@RequestMapping("/hackaton/v1")
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    LoginService loginService;

    @GetMapping("/users")
    public ResponseEntity<ResultModel> getUsers(@RequestHeader String token){
        System.out.println("Controller - getUsers");

        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null){
            ResultModel result =  userService.findAllUser();
            return new ResponseEntity<>(result, result.getStatus());
        }else{
            ResultModel result = new ResultModel();
            result.setResultType(9);
            result.setStatus(HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(result, result.getStatus());
        }

    }

    @GetMapping("/users/{id}")
    public ResponseEntity<ResultModel> getUserById(@RequestHeader String token, @PathVariable String id){
        System.out.println("Controller - getUserById");
        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null) {
            ResultModel result =  userService.findByUser(id);
            return new ResponseEntity<>(result, result.getStatus());
        }else{
            ResultModel result = new ResultModel();
            result.setResultType(9);
            result.setStatus(HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(result, result.getStatus());
        }
    }

    @PostMapping("/users")
    public ResponseEntity<ResultModel> createUser(@RequestHeader String token, @RequestBody UserModel user){
        System.out.println("Controller - createUser");

        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null && login.getProfile().toUpperCase().equals("ADMIN")){
            ResultModel result = userService.upsertUser(user);
            return new ResponseEntity<>(result, result.getStatus());
        }else{
            ResultModel result = new ResultModel();
            result.setResultType(9);
            result.setStatus(HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(result, result.getStatus());
        }

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<ResultModel> updateUser(@RequestHeader String token, @PathVariable String id, @RequestBody UserModel user){
        System.out.println("Controller - updateUser");
        LoginModel login = loginService.loginControl(token);
        if (login.getUserId()!=null && login.getProfile().toUpperCase().equals("ADMIN")){
            if (id.equals(user.getId())){
                ResultModel userFound = userService.findByUser(user.getId());
                if (userFound.getResultType()==0){
                    ResultModel result =  userService.upsertUser(user);
                    return new ResponseEntity<>(result, result.getStatus());

                }else{
                    ResultModel result = new ResultModel();
                    result.setObjectModel(new UserModel());
                    result.setResultType(1);
                    result.setStatus(HttpStatus.NOT_FOUND);
                    return new ResponseEntity<>(result, result.getStatus());
                }
            }else{
                ResultModel result = new ResultModel();
                result.setObjectModel(new UserModel());
                result.setResultType(9);
                result.setStatus(HttpStatus.BAD_REQUEST);
                return new ResponseEntity<>(result, result.getStatus());
            }
        }else{
            ResultModel result = new ResultModel();
            result.setResultType(9);
            result.setStatus(HttpStatus.FORBIDDEN);
            return new ResponseEntity<>(result, result.getStatus());
        }
    }
}
