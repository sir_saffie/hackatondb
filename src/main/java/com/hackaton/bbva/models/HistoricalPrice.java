package com.hackaton.bbva.models;

import java.util.Map;

public class HistoricalPrice {
    private String idProduct;
    private String desc;
    private Map precio; //Data->Price

    public HistoricalPrice() {
    }

    public HistoricalPrice(String idProduct, String desc, Map precio) {
        this.idProduct = idProduct;
        this.desc = desc;
        this.precio = precio;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Map getPrecio() {
        return precio;
    }

    public void setPrecio(Map precio) {
        this.precio = precio;
    }
}
