package com.hackaton.bbva.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.util.Date;


@Document(collection = "login")
public class LoginModel {
    @Id
    private String userId;
    private String token;
    private String profile;
    private Date audit_login;
    private Date audit_last_access;

    public LoginModel() {
    }

    public LoginModel(String userId, String token, String profile, Timestamp audit_login, Timestamp audit_last_access) {
        this.userId = userId;
        this.token = token;
        this.profile = profile;
        this.audit_login = audit_login;
        this.audit_last_access = audit_last_access;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Date getAudit_login() {
        return audit_login;
    }

    public void setAudit_login(Date audit_login) {
        this.audit_login = audit_login;
    }

    public Date getAudit_last_access() {
        return audit_last_access;
    }

    public void setAudit_last_access(Date audit_last_access) {
        this.audit_last_access = audit_last_access;
    }
}
