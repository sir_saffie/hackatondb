package com.hackaton.bbva.models;

public class ProductInputModel {

    private String id;
    private String descripcion;
    private String categoria;
    private Integer valor;
    private String productId;
    private Float price;

    public ProductInputModel() {
    }

    public ProductInputModel(String id, String descripcion, String categoria, Integer valor, String productId, Float price) {
        this.id = id;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.valor = valor;
        this.productId = productId;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
