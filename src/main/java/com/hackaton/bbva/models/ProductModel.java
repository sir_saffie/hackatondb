package com.hackaton.bbva.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products")
public class ProductModel {

    @Id
    private String id;
    private String descripcion;
    private String categoria;
    private Integer valor;

    public ProductModel() {
    }

    public ProductModel(String id, String descripcion, String categoria, Integer valor) {
        this.id = id;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.valor = valor;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getdescripcion() {
        return descripcion;
    }
    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getcategoria() {
        return categoria;
    }
    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }

    public Integer getValor() {
        return valor;
    }
    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
