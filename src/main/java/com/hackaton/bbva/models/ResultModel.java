package com.hackaton.bbva.models;

import org.springframework.http.HttpStatus;

public class ResultModel {

    private Object objectModel;
    private HttpStatus status;
    private int resultType;

    public ResultModel() {
    }


    public ResultModel(Object objectModel, HttpStatus status, int resultType) {
        this.objectModel = objectModel;
        this.status = status;
        this.resultType = resultType;
    }

    public Object getObjectModel() {
        return objectModel;
    }

    public void setObjectModel(Object objectModel) {
        this.objectModel = objectModel;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }


    public int getResultType() {
        return resultType;
    }

    public void setResultType(int resultType) {
        this.resultType = resultType;
    }
}
