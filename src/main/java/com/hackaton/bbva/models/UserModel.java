package com.hackaton.bbva.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
public class UserModel {
    @Id
    private String id;
    private String nickname;
    private String password;
    private String profile;
    private String email;

    public UserModel() {
    }

    public UserModel(String id, String nickname, String password, String profile, String email) {
        this.id = id;
        this.nickname = nickname;
        this.password = password;
        this.profile = profile;
        this.email = email;
    }

    public UserModel(String nickname, String password) {
        this.nickname = nickname;
        this.password = password;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile() {
        return this.profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
