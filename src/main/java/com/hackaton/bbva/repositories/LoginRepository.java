package com.hackaton.bbva.repositories;

import com.hackaton.bbva.models.LoginModel;
import com.hackaton.bbva.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends MongoRepository<LoginModel,String> {
    @Query("{ 'token' : ?0 }")
    public LoginModel findByToken(String token);
}
