package com.hackaton.bbva.repositories;

import com.hackaton.bbva.models.PriceModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.Date;
import java.util.List;

@Repository
public interface PriceRepository extends MongoRepository<PriceModel,String> {

    List<PriceModel> findByProductId(String productId);

    @Query("{'productId' : ?0 , 'endDate' : { $gte: ?1 } }")
    List<PriceModel> findProductLastPriceByEndDate(String productId, Date endDate);

//    @Query("{'startDate' : { $gte: ?0, $lte: ?1 } }")
    @Query(value="{'productId' : ?0 , 'startDate' : { $gte: ?1, $lte: ?2 }}",sort = "{'startDate' : -1}")
    List<PriceModel> findStartDateBetween(String productId, Date from, Date to);

    @Query("{'productId' : ?0 , 'startDate' : { $gte: ?1 } }")
    List<PriceModel> findProductPricesByDate(String productId, Date startDate);

}
