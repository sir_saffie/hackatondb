package com.hackaton.bbva.repositories;

import com.hackaton.bbva.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel,String> {
    @Query("{ 'nickname' : ?0 }")
    public UserModel findByNickname(String nickname);
}
