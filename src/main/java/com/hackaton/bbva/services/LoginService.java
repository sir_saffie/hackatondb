package com.hackaton.bbva.services;

import com.hackaton.bbva.models.LoginModel;
import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.models.UserModel;
import com.hackaton.bbva.repositories.LoginRepository;
import com.hackaton.bbva.repositories.UserRepository;
import org.apache.juli.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class LoginService {
    @Autowired
    LoginRepository loginRepository;

    @Autowired
    UserService userService;

    public List<LoginModel> findAll(){
        System.out.println("LoginService - findAll");
        return loginRepository.findAll();
    }
    public LoginModel  loginControl(String token){
        System.out.println("LoginService - getLogin");

        LoginModel login = loginRepository.findByToken(token);
        if (login != null){
            Date date = new Date();
            Date timestamp = new Date(date.getTime());
            login.setAudit_last_access(timestamp);
            loginRepository.save(login);
            return login;
        }else{
            return new LoginModel();
        }
    }
    public ResultModel createLogin(UserModel user){
        System.out.println("LoginService - createLogin");
        UserModel userOK = userService.autenticacionUsuario(user);
        LoginModel login = new LoginModel();
        ResultModel result = new ResultModel();
        if (userOK.getId()!= null){
            login.setUserId(userOK.getId());
            login.setProfile(userOK.getProfile());
            Date date = new Date();
            Date timestamp = new Date(date.getTime());
            login.setAudit_login(timestamp);
            login.setAudit_last_access(timestamp);
            login.setToken(generateToken());
            loginRepository.save(login);
            result.setObjectModel(login);
            result.setResultType(0);
            result.setStatus(HttpStatus.OK);
        }else{
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setResultType(1);
            result.setObjectModel(login);
        }

        return result;
    }

    private String generateToken(){
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(10);

        for (int i = 0; i < 10; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }
}
