package com.hackaton.bbva.services;

import com.hackaton.bbva.models.PriceModel;
import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.repositories.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PriceService {
    @Autowired
    PriceRepository priceRepository;

    @Autowired
    ProductService productService;

    public List<PriceModel> findAllPrices() {
        System.out.println("PriceService.findAllPrices");

        return this.priceRepository.findAll();
    }

    public Optional<PriceModel> findPriceById(String id) {
        System.out.println("PriceService.findPriceById");

        return this.priceRepository.findById(id);
    }
// gets all prices by product ID
    public ResultModel findPricesByProductId(String productId) {
        System.out.println("PriceService.findPricesByProductId");

        ResultModel resultModel = new ResultModel();

        List<PriceModel> priceList = this.priceRepository.findByProductId(productId);
        if (priceList.isEmpty()) {
            resultModel.setStatus(HttpStatus.NOT_FOUND);
            resultModel.setResultType(1);
        } else {
            resultModel.setObjectModel(priceList);
            resultModel.setStatus(HttpStatus.OK);
            resultModel.setResultType(0);
        }
        return resultModel;
    }
//get prices from a product with start date greater or equal than a date
    public ResultModel findProductPricesByDate(String productId, String startDate) {
        System.out.println("PriceService.findProductPricesByDate");

        ResultModel resultModel = new ResultModel();


        List<PriceModel> priceList = null;

        try {
            priceList = this.priceRepository.findProductPricesByDate(productId,this.StringToDate(startDate));
            if (priceList.isEmpty()) {
                resultModel.setStatus(HttpStatus.NOT_FOUND);
                resultModel.setResultType(1);
            } else {
                resultModel.setObjectModel(priceList);
                resultModel.setStatus(HttpStatus.OK);
                resultModel.setResultType(0);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    //get prices from one product which startDate is between 2 dates
    public ResultModel findProductPricesBetweenDates(String productId, String startDate, String endDate) {
        System.out.println("PriceService.findProductPricesBetweenDates");
        System.out.println("productId: " + productId);
        System.out.println("startDate: " + startDate);
        System.out.println("endDate: " + endDate);

        ResultModel resultModel = new ResultModel();

        List<PriceModel> priceList = null;

        try {
            priceList = this.priceRepository.findStartDateBetween(productId,this.StringToDate(startDate),this.StringToDate(endDate));
            if (priceList.isEmpty()) {
                resultModel.setStatus(HttpStatus.NOT_FOUND);
                resultModel.setResultType(1);
            } else {
                resultModel.setObjectModel(priceList);
                resultModel.setStatus(HttpStatus.OK);
                resultModel.setResultType(0);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return resultModel;
    }

    public ResultModel addPrice(PriceModel newPrice) {
        System.out.println("PriceService.addPrice");
        System.out.println("id: " + newPrice.getId());
        System.out.println("productId: " + newPrice.getProductId());
        System.out.println("price: " + newPrice.getPrice());

        ResultModel resultModel = new ResultModel();

        if (this.productService.findProductById(newPrice.getProductId()).isPresent())
        {
            try {
                List<PriceModel> lastPrices = this.priceRepository.findProductLastPriceByEndDate(newPrice.getProductId(), this.StringToDate("2999-12-31"));
                if (!lastPrices.isEmpty()) {
                    PriceModel lastPrice = lastPrices.get(0);
                    if (newPrice.getStartDate() == null){
                        lastPrice.setEndDate(new Date());
                    } else {
                        lastPrice.setEndDate(newPrice.getStartDate());
                    }
                    this.priceRepository.save(lastPrice);
                }
                newPrice.setId(null);
                if (newPrice.getStartDate() == null){
                    newPrice.setStartDate(new Date());
                }
                newPrice.setEndDate(this.StringToDate("2999-12-31"));
                this.priceRepository.save(newPrice);

                resultModel.setObjectModel(newPrice);
                resultModel.setStatus(HttpStatus.CREATED);
                resultModel.setResultType(0);
            } catch (ParseException e) {
                e.printStackTrace();
                resultModel.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                resultModel.setResultType(9);
            }

        } else {
            resultModel.setStatus(HttpStatus.NOT_FOUND);
            resultModel.setResultType(1);
        }

        return resultModel;
    }

    public Date StringToDate(String dateInString) throws ParseException {
        System.out.println("stringToDate");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        return formatter.parse(dateInString);
    }

}
