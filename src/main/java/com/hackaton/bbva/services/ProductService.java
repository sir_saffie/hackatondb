package com.hackaton.bbva.services;

import com.hackaton.bbva.models.*;
import com.hackaton.bbva.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PriceService priceService;

    public ResultModel findAllProduct() {
        System.out.println("findAll en ProductService");
        ResultModel resultModel = new ResultModel();
        resultModel.setObjectModel(this.productRepository.findAll());
        resultModel.setStatus(HttpStatus.OK);
        resultModel.setResultType(0);
        return resultModel;
    }

    public Optional<ProductModel> findProductById(String id) {
        System.out.println("findProductById en ProductService");
        return this.productRepository.findById(id);
    }

    public ResultModel addProduct(ProductInputModel newProduct) {
        System.out.println("addProduct en ProductService");
        ResultModel resultModel = new ResultModel();
        ProductModel productModel = new ProductModel();
        PriceModel priceModel = new PriceModel();

        productModel.setId(newProduct.getId());
        productModel.setcategoria(newProduct.getCategoria());
        productModel.setdescripcion(newProduct.getDescripcion());
        productModel.setValor(newProduct.getValor());

        priceModel.setProductId(newProduct.getId());
        priceModel.setPrice(newProduct.getPrice());

        this.productRepository.save(productModel);
        this.priceService.addPrice(priceModel);

        resultModel.setObjectModel(newProduct);
        resultModel.setStatus(HttpStatus.CREATED);
        resultModel.setResultType(0);

        return resultModel;
    }

    public ResultModel updateProductById(ProductInputModel newProduct) {
        System.out.println("updateProduct en ProductService");
        ResultModel resultModel = new ResultModel();
        ProductModel productModel = new ProductModel();
        PriceModel priceModel = new PriceModel();

        if (this.productRepository.findById(newProduct.getId()).isPresent()) {
            productModel.setId(newProduct.getId());
            productModel.setcategoria(newProduct.getCategoria());
            productModel.setdescripcion(newProduct.getDescripcion());
            productModel.setValor(newProduct.getValor());
            this.productRepository.save(productModel);

            if (newProduct.getPrice() != null) {
                priceModel.setProductId(newProduct.getId());
                priceModel.setPrice(newProduct.getPrice());
                this.priceService.addPrice(priceModel);
            }
            resultModel.setObjectModel(newProduct);
            resultModel.setStatus(HttpStatus.OK);
            resultModel.setResultType(0);
        } else {
            resultModel.setObjectModel("PRODUCT NOT FOUND");
            resultModel.setStatus(HttpStatus.NOT_FOUND);
            resultModel.setResultType(1);
        }
        return  resultModel;
    }

    public ResultModel getHistoricalPrice(String id,String start_date,String end_date){
        Optional<ProductModel> product = findProductById(id);
        if (product.isPresent()){
            ResultModel result = priceService.findProductPricesBetweenDates(id,start_date,end_date);

            List<PriceModel> historicalPrices = (List<PriceModel>) result.getObjectModel();
            HistoricalPrice historical = new HistoricalPrice();
            historical.setIdProduct(id);
            historical.setDesc(product.get().getdescripcion());
            Map<Date, Float> prices = new HashMap<>();
            System.out.println("precios: " + historicalPrices.size());
            historicalPrices.forEach(price-> {
                prices.put(price.getStartDate(),price.getPrice());
            });
            historical.setPrecio(prices);
            result.setObjectModel(historical);
            return result;
        }else{
            ResultModel result = new ResultModel();
            result.setObjectModel(new UserModel());
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setResultType(1);
            return result;
        }
    }
}