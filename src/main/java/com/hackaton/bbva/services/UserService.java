package com.hackaton.bbva.services;

import com.hackaton.bbva.models.LoginModel;
import com.hackaton.bbva.models.ResultModel;
import com.hackaton.bbva.models.UserModel;
import com.hackaton.bbva.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public ResultModel findAllUser(){
        System.out.println("Service - findAllUser");


        List<UserModel> userList =  userRepository.findAll();
        ResultModel result = new ResultModel();
        result.setObjectModel(userList);
        result.setStatus(HttpStatus.OK);
        result.setResultType(0);
        return result;
    }

    public ResultModel findByUser(String id){
        System.out.println("Service - findByUser");

        Optional<UserModel> user =  userRepository.findById(id);
        ResultModel result = new ResultModel();
        if (user.isPresent()){
            result.setObjectModel(user);
            result.setStatus(HttpStatus.OK);
            result.setResultType(0);
        }else{
            result.setObjectModel(new UserModel());
            result.setStatus(HttpStatus.NOT_FOUND);
            result.setResultType(1);
        }
        return result;

    }

    public ResultModel upsertUser(UserModel user){
        userRepository.save(user);
        ResultModel result = new ResultModel();
        result.setObjectModel(user);
        result.setStatus(HttpStatus.OK);
        result.setResultType(0);
        return result;
    }

    public UserModel autenticacionUsuario(UserModel user){
        System.out.println("UserService - autenticacionUsuario");
        UserModel userNick =  userRepository.findByNickname(user.getNickname());
        if (userNick != null){
            if (userNick.getPassword().equals(user.getPassword())){
                return userNick;
            }else{
                return new UserModel();
            }
        }else{
            return new UserModel();
        }

    }

}
